//
//  Utility.swift
//  Smart Wallet
//
//  Created by Elia Pasqualini on 13/05/2019.
//  Copyright © 2019 Progetto Mobile 2019. All rights reserved.
//

import Foundation
import Firebase

class Utility {
    static func messageErrorFromCode(code: Int) -> String {
        var message = ""
        if let errCode = AuthErrorCode(rawValue: code) {
            
            switch errCode {
                case .missingEmail:
                    message = "Email mancante"
                case .invalidEmail:
                    message = "Email non valida"
                case .emailAlreadyInUse:
                    message = "Email già in uso"
                case .weakPassword:
                    message = "La password deve contenere almeno sei caratteri"
                case .wrongPassword:
                    message = "Password non valida"
                default:
                    message = "Errore, controlla che i campi siano corretti"
            }
        }
        return message
    }
    
    static func getCurrentMonth() -> String {
        let date = Date()
        let calendar = Calendar.current
        return getMonthName(calendar.component(.month, from: date))
    }
    
    static func getMonthYear() -> String {
        let date = Date()
        let calendar = Calendar.current
        return getMonthName(calendar.component(.month, from: date)) + "-" + String(calendar.component(.year, from: date))
    }
    
    static func getMonthYear(date: Date) -> String {
        let calendar = Calendar.current
        return getMonthName(calendar.component(.month, from: date)) + "-" + String(calendar.component(.year, from: date))
    }
    
    static func getMonthName(_ number: Int) -> String {
        var month: String = ""
        switch number {
        case 1:
            month = "Gennaio"
        case 2:
            month = "Febbraio"
        case 3:
            month = "Marzo"
        case 4:
            month = "Aprile"
        case 5:
            month = "Maggio"
        case 6:
            month = "Giugno"
        case 7:
            month = "Luglio"
        case 8:
            month = "Agosto"
        case 9:
            month = "Settembre"
        case 10:
            month = "Ottobre"
        case 11:
            month = "Novembre"
        case 12:
            month = "Dicembre"
        default:
            month = "NONE"
        }
        
        return month
    }
    
    static func getMonthNameItalia(_ name: String) -> String {
        var month: String = ""
        switch name {
        case "January":
            month = "Gennaio"
        case "February":
            month = "Febbraio"
        case "March":
            month = "Marzo"
        case "April":
            month = "Aprile"
        case "May":
            month = "Maggio"
        case "June":
            month = "Giugno"
        case "July":
            month = "Luglio"
        case "August":
            month = "Agosto"
        case "September":
            month = "Settembre"
        case "October":
            month = "Ottobre"
        case "November":
            month = "Novembre"
        case "December":
            month = "Dicembre"
        default:
            month = "NONE"
        }
        
        return month
    }
}
