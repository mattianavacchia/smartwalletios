//
//  OcrManager.swift
//  Smart Wallet
//
//  Created by Elia Pasqualini on 06/06/2019.
//  Copyright © 2019 Progetto Mobile 2019. All rights reserved.
//

import Foundation
import Firebase
import FirebaseMLCommon

class OcrManager {
    
    private var totalAmount :String?
    private var shopName :String?
    private var shopAddress :String?
    private var details :String?
    private var date :String?
    private var dateFormatterGet = DateFormatter()
    
    public func processTotal(result : VisionText) -> String {
        var top : CGFloat = 0
        var bottomRight : CGFloat = 10000
        self.totalAmount = ""
        
        for block in result.blocks{
            for line in block.lines{
                let lineFrame = line.frame
                for element in line.elements{
                    let elementText = element.text
                    if elementText.caseInsensitiveCompare("TOTALE") == .orderedSame || elementText.caseInsensitiveCompare("IMPORTO") == .orderedSame {
                        top = lineFrame.maxY
                    }
                }
            }
        }
        for block in result.blocks {
            for line in block.lines {
                let lineFrame = line.frame
                for element in line.elements {
                    let elementText = element.text
                    let elementFrame = element.frame
                    if (lineFrame.maxY > top - 20 && lineFrame.maxY < top + 20) {
                        if elementFrame.minY <  bottomRight && elementText.trim().isNumeric {
                            self.totalAmount = elementText
                            bottomRight = lineFrame.minY
                            
                        }
                    }
                }
            }
        }
        if self.totalAmount == "" {
            self.totalAmount = "0.00"
        }
        return self.totalAmount!
    }
    
    
    public func getName(result : VisionText) -> String {
        
        var top : CGFloat = 10000
        var firstBlock : CGFloat = 10000
        self.shopName = ""
        for block in result.blocks {
            let blockFrame = block.frame
            for line in block.lines {
                for element in line.elements {
                    let elementText = element.text
                    if blockFrame.minY < firstBlock {
                        firstBlock = blockFrame.minY
                    }
                    if elementText.caseInsensitiveCompare("Via") == .orderedSame || elementText.caseInsensitiveCompare("Strada") == .orderedSame || elementText.caseInsensitiveCompare("Viale") == .orderedSame || elementText.caseInsensitiveCompare("Piazza") == .orderedSame || elementText.caseInsensitiveCompare("Corso") == .orderedSame {
                        if blockFrame.minY < top {
                            top = blockFrame.minY
                        }
                    }
                }
            }
        }
        for block in result.blocks{
            for line in block.lines{
                let lineFrame = line.frame
                let lineText = line.text
                if lineFrame.minY < top  && lineText != self.shopAddress {
                    self.shopName = lineText
                }
                
            }
        }
        
        if self.shopName == ""{
            var lineText = ""
            for block in result.blocks{
                for line in block.lines{
                    if lineText == "" {
                        lineText = line.text
                        self.shopName = lineText
                    }
                }
            }
        }
        return self.shopName!
        
    }
    
    public func getAddress(result : VisionText) -> String {
        var top : CGFloat = 10000
        self.shopAddress = ""
        
        for block in result.blocks{
            let blockFrame = block.frame
            for line in block.lines{
                let lineText = line.text
                for element in line.elements{
                    let elementText = element.text
                    if elementText.caseInsensitiveCompare("Via") == .orderedSame || elementText.caseInsensitiveCompare("Strada") == .orderedSame || elementText.caseInsensitiveCompare("Viale") == .orderedSame || elementText.caseInsensitiveCompare("Piazza") == .orderedSame || elementText.caseInsensitiveCompare("Corso") == .orderedSame {
                        if blockFrame.maxY < top {
                            top = blockFrame.maxY
                            self.shopAddress = lineText
                        }
                    }
                }
            }
        }
        
        return self.shopAddress!
    }
    
    public func getDate(result : VisionText) -> String {
        self.date = ""
        var dateFormatters = ["dd-MM-yyyy", "dd/MM/yyyy", "yyyy-MM-dd", "yyyy/MM/dd", "MM-dd-yyyy","MM/dd/yyyy"]
        for block in result.blocks{
            for line in block.lines{
                for element in line.elements{
                    let elementText = element.text
                    let dateFormatterGet = DateFormatter()
                    for n in 0 ... 5 {
                        dateFormatterGet.dateFormat = dateFormatters[n]
                        if dateFormatterGet.date(from: elementText) != nil {
                            self.date = elementText
                        }
                    }
                }
                
            }
        }
        return self.date!
    }
    
    public func getDetails(result : VisionText) -> String {
        var topAddress : CGFloat = 10000
        var bottomAddress : CGFloat = 10000
        var topTot : CGFloat = 10000
        self.details = ""
        var temp = [Int : String]()
        
        for block in result.blocks{
            let blockFrame = block.frame
            for line in block.lines{
                for element in line.elements{
                    let elementText = element.text
                    if elementText.caseInsensitiveCompare("Via") == .orderedSame || elementText.caseInsensitiveCompare("Strada") == .orderedSame || elementText.caseInsensitiveCompare("Viale") == .orderedSame || elementText.caseInsensitiveCompare("Piazza") == .orderedSame || elementText.caseInsensitiveCompare("Corso") == .orderedSame {
                        if blockFrame.maxY < topAddress {
                            
                            topAddress = blockFrame.minY
                            bottomAddress = blockFrame.maxY
                        }
                    }
                    if elementText.caseInsensitiveCompare("TOTALE") == .orderedSame || elementText.caseInsensitiveCompare("IMPORTO") == .orderedSame {
                        topTot = blockFrame.maxY
                    }
                }
            }
        }
        
        for block in result.blocks {
            let blockFrame = block.frame
            for line in block.lines {
                let lineFrame = line.frame
                let lineText = line.text
                if blockFrame.minY > bottomAddress && blockFrame.maxY < topTot - 20 {
                    let currentLine : Int = Int(lineFrame.minY)
                    temp[currentLine] = lineText + " "
                    for (integer, word) in temp {
                        if currentLine > integer - 10 && currentLine < integer + 10 {
                            var tmp : String = ""
                            tmp = word
                            if tmp.contains(lineText) == false{
                                tmp = tmp + "" + lineText
                                temp.removeValue(forKey: integer)
                                temp.removeValue(forKey: currentLine)
                                if temp.values.contains(lineText) == false {
                                    temp[integer] = tmp
                                }
                            }
                        }
                    }
                }
            }
        }
        for i in temp.keys {
            self.details = self.details! + temp[i]! + "\n"
        }
        
        return self.details!
    }
    
}

extension String {
    var isNumeric: Bool {
        guard self.count > 0 else { return false }
        let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9",",","."]
        return Set(self).isSubset(of: nums)
    }
    
    func trim() -> String
    {
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
    }
}
