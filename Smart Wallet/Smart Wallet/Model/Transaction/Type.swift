//
//  Type.swift
//  Smart Wallet
//
//  Created by Mattia Navach on 29/05/2019.
//  Copyright © 2019 Progetto Mobile 2019. All rights reserved.
//

import Foundation

enum Type : String {
    case INCOME = "INCOME"
    case EXPENSES = "EXPENSES"
}
