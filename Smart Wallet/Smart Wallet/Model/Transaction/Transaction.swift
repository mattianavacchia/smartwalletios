//
//  Transaction.swift
//  Smart Wallet
//
//  Created by Mattia Navach on 29/05/2019.
//  Copyright © 2019 Progetto Mobile 2019. All rights reserved.
//

import Foundation

class Transaction {
    private var id: String;
    private var type: Type;
    private var name: String;
    private var shopName: String;
    private var shopAddress: String;
    private var category: String;
    private var date: String;
    private var description: String;
    private var amount: Double;
    private var pathPhoto: String;
    
    public init() {
        self.id = UUID().uuidString
        self.type = Type.EXPENSES
        self.name = ""
        self.shopName = ""
        self.shopAddress = ""
        self.category = ""
        self.date = ""
        self.description = ""
        self.amount = 0.00
        self.pathPhoto = ""
    }
    
    public func getID() -> String {
        return self.id
    }
    
    public func setID(id: String) {
        self.id = id
    }
    
    public func setType(type: Type) -> Void {
        self.type = type;
    }
    
    public func getType() -> Type {
        return self.type
    }
    
    public func setName(name: String) -> Void {
        self.name = name;
    }
    
    public func getName() -> String {
        return self.name
    }
    
    public func setShopName(shopName: String) -> Void {
        self.shopName = shopName;
    }
    
    public func getshopName() -> String {
        return self.shopName
    }
    
    public func setShopAddress(shopAddress: String) -> Void {
        self.shopAddress = shopAddress;
    }
    
    public func getshopAddress() -> String {
        return self.shopAddress
    }
    
    public func setCategory(category: String) -> Void {
        self.category = category;
    }
    
    public func getCategory() -> String {
        return self.category
    }
    
    public func setDate(date: String) -> Void {
        self.date = date;
    }
    
    public func getDate() -> String {
        return self.date
    }
    
    public func setDescription(description: String) -> Void {
        self.description = description;
    }
    
    public func getDescription() -> String {
        return self.description
    }
    
    public func setAmount(amount: Double) -> Void {
        self.amount = amount
    }
    
    public func getAmount() -> Double {
        return self.amount
    }
    
    public func setPathPhoto(pathPhoto: String) -> Void {
        self.pathPhoto = pathPhoto;
    }
    
    public func getPathPhoto() -> String {
        return self.pathPhoto
    }
}
