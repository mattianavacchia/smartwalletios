//
//  TransactionManager.swift
//  Smart Wallet
//
//  Created by Mattia Navach on 29/05/2019.
//  Copyright © 2019 Progetto Mobile 2019. All rights reserved.
//

import Foundation

class TransactionManager {
    
    private static var currentTransaction: Transaction?
    private static var typeOperation: TypeOperation = TypeOperation.EMPTY
    private static var amountOfTheMonth: Double = 0.00
    private static var amount = 0.00
    
    public static func initializeTransaction() -> Void {
        currentTransaction = Transaction()
    }
    
    public static func clearTransaction() {
        self.initializeTransaction()
    }
    
    public static func checkImportantFields() -> Bool {
        return TransactionManager.getAmount() > 0.00
                && !(TransactionManager.getCategory() == "")
                && !(TransactionManager.getDate() == "")
                && !(TransactionManager.getType() == "")
    }
    
    public static func setCurrentTransaction(t: Transaction) {
        if currentTransaction == nil {
            self.initializeTransaction()
        }
        amount = t.getAmount()
        currentTransaction! = t
    }
    
    public static func getCurrentTransaction() -> Transaction {
        return currentTransaction!
    }
    
    public static func hasChanged() -> Bool {
        return amount != currentTransaction?.getAmount()
    }
    
    public static func getAmountBeforeCurrent() -> Double {
        return amount
    }
    
    public static func setAmountOfTheMonth(amount: Double) {
        amountOfTheMonth = amount
    }
    
    public static func getAmountOfTheMonth() -> Double {
        return amountOfTheMonth
    }
    
    public static func setTypeOperation(op: TypeOperation) {
        typeOperation = op
    }
    
    public static func getTypeOperation() -> TypeOperation {
        return typeOperation
    }
    
    public static func getID() -> String {
        return (currentTransaction?.getID())!
    }
    
    public static func getDescription() -> String {
        return (currentTransaction?.getDescription())!
    }
    
    public static func getPathPhoto() -> String {
        return (currentTransaction?.getPathPhoto())!
    }
    
    public static func getType() -> String {
        return (currentTransaction?.getType().rawValue)!
    }
    
    public static func setCategory(category: String) {
        currentTransaction?.setCategory(category: category)
    }
    
    public static func getCategory() -> String {
        return (currentTransaction?.getCategory())!
    }
    
    public static func setType(type: Type) {
        currentTransaction?.setType(type: type)
    }
    
    public static func setAmount(amount: Double) {
        currentTransaction?.setAmount(amount: amount)
    }
    
    public static func getAmount() -> Double {
        return (currentTransaction?.getAmount())!
    }
    
    public static func setName(name: String) {
        currentTransaction?.setName(name: name)
    }
    
    public static func getName() -> String {
        return (currentTransaction?.getName())!
    }
    
    public static func setShopName(shopName: String) {
        currentTransaction?.setShopName(shopName: shopName)
    }
    
    public static func getShopName() -> String {
        return (currentTransaction?.getshopName())!
    }
    
    public static func setShopAddress(shopAddress: String) {
        currentTransaction?.setShopAddress(shopAddress: shopAddress)
    }
    
    public static func getShopAddress() -> String {
        return (currentTransaction?.getshopAddress())!
    }
    
    public static func setDate(date: String) {
        currentTransaction?.setDate(date: date)
    }
    
    public static func getDate() -> String {
        return (currentTransaction?.getDate())!
    }
    
    public static func setDescription(description: String) {
        currentTransaction?.setDescription(description: description)
    }
    
    public static func setPathPhoto(path: String) {
        currentTransaction?.setPathPhoto(pathPhoto: path)
    }
}
