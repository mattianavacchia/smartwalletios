//
//  CategoryExpenses.swift
//  Smart Wallet
//
//  Created by Mattia Navach on 29/05/2019.
//  Copyright © 2019 Progetto Mobile 2019. All rights reserved.
//

import Foundation

enum CategoryExpenses: CaseIterable {
    case TAX
    case FOOD
    case HOLIDAY
    case GIFT
    case PURCHASE
    case FAMILY
    case HOBBY
    case TRANSPORT
    case OTHER
    
    public static func getResourse(category: CategoryExpenses) -> String {
        var resource: String
        switch category {
            case .TAX:
                resource = "icon_tax"
            case .FOOD:
                resource = "icon_food"
            case .HOLIDAY:
                resource = "icon_holiday"
            case .GIFT:
                resource = "icon_gift"
            case .PURCHASE:
                resource = "icon_purchase"
            case .FAMILY:
                resource = "icon_family"
            case .HOBBY:
                resource = "icon_hobby"
            case .TRANSPORT:
                resource = "icon_transport"
            case .OTHER:
                resource = "icon_other"
        }
        
        return resource
    }
    
    public static func getResourseFromString(category: String) -> String {
        var resource: String
        switch category {
        case "TAX":
            resource = "icon_tax"
        case "FOOD":
            resource = "icon_food"
        case "HOLIDAY":
            resource = "icon_holiday"
        case "GIFT":
            resource = "icon_gift"
        case "PURCHASE":
            resource = "icon_purchase"
        case "FAMILY":
            resource = "icon_family"
        case "HOBBY":
            resource = "icon_hobby"
        case "TRANSPORT":
            resource = "icon_transport"
        case "OTHER":
            resource = "icon_other"
        default: resource = ""
        }
        
        return resource
    }
    
    public static func getName(category: CategoryExpenses) -> String {
        var name: String
        switch category {
        case .TAX:
            name = "TAX"
        case .FOOD:
            name = "FOOD"
        case .HOLIDAY:
            name = "HOLIDAY"
        case .GIFT:
            name = "GIFT"
        case .PURCHASE:
            name = "PURCHASE"
        case .FAMILY:
            name = "FAMILY"
        case .HOBBY:
            name = "HOBBY"
        case .TRANSPORT:
            name = "TRANSPORT"
        case .OTHER:
            name = "OTHER"
        }
        
        return name
    }
}
