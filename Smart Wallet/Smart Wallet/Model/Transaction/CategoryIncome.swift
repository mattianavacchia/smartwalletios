//
//  CategoryIncome.swift
//  Smart Wallet
//
//  Created by Mattia Navach on 29/05/2019.
//  Copyright © 2019 Progetto Mobile 2019. All rights reserved.
//

import Foundation

enum CategoryIncome: CaseIterable{
    case GIFT
    case OTHER
    case SALARY
    case RENT
    case REFUND
    
    public static func getResourse(category: CategoryIncome) -> String {
        var resource: String
        switch category {
        case .GIFT:
            resource = "icon_gift"
        case .OTHER:
            resource = "icon_other"
        case .SALARY:
            resource = "icon_salary"
        case .RENT:
            resource = "icon_rent"
        case .REFUND:
            resource = "icon_refund"
        }
        
        return resource
    }
    
    public static func getResourseFromString(category: String) -> String {
        var resource: String
        switch category {
        case "GIFT":
            resource = "icon_gift"
        case "OTHER":
            resource = "icon_other"
        case "SALARY":
            resource = "icon_salary"
        case "RENT":
            resource = "icon_rent"
        case "REFUND":
            resource = "icon_refund"
        default:
            resource = ""
        }
        
        return resource
    }
    
    public static func getName(category: CategoryIncome) -> String {
        var name: String
        switch category {
        case .GIFT:
            name = "GIFT"
        case .OTHER:
            name = "OTHER"
        case .SALARY:
            name = "SALARY"
        case .RENT:
            name = "RENT"
        case .REFUND:
            name = "REFUND"
        }
        
        return name
    }
}
