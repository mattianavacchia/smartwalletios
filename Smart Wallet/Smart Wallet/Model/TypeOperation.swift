//
//  TypeOperation.swift
//  Smart Wallet
//
//  Created by Mattia Navach on 05/06/2019.
//  Copyright © 2019 Progetto Mobile 2019. All rights reserved.
//

import Foundation

enum TypeOperation {
    case EMPTY
    case OCR
    case MODIFY
}
