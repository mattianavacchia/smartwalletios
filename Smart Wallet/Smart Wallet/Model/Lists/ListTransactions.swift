//
//  List.swift
//  Smart Wallet
//
//  Created by Mattia Navach on 22/06/2019.
//  Copyright © 2019 Progetto Mobile 2019. All rights reserved.
//

import Foundation

public class ListTransactions {
    private var id: String
    private var name: String
    
    public init() {
        id = ""
        name = ""
    }
    
    public func getId() -> String {
    return id;
    }
    
    public func setId(id: String) {
        self.id = id
    }
    
    public func getName() -> String {
        return name
    }
    
    public func setName(name: String) {
        self.name = name
    }
}
