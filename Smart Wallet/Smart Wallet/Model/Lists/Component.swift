//
//  Component.swift
//  Smart Wallet
//
//  Created by Mattia Navach on 22/06/2019.
//  Copyright © 2019 Progetto Mobile 2019. All rights reserved.
//

import Foundation

public class Component {
    private var id: String
    private var email: String
    
    init() {
        id = ""
        email = ""
    }
    
    public func getId() -> String {
        return id
    }
    
    public func setId(id: String) {
        self.id = id
    }
    
    public func getEmail() -> String {
        return email
    }
    
    public func setEmail(email: String) {
        self.email = email
    }
}
