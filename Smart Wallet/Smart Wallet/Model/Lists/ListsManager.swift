//
//  ListsManager.swift
//  Smart Wallet
//
//  Created by Mattia Navach on 14/06/2019.
//  Copyright © 2019 Progetto Mobile 2019. All rights reserved.
//

import Foundation

public class ListsManager {
    enum State { case NONE; case DOING }
    
    private static var state: State = State.NONE
    private static var currentList = ListTransactions()
    
    init() {
    }
    
    public static func initiliazeListManager() {
        currentList = ListTransactions()
    }
    
    public static func getCurrentList() -> ListTransactions {
        return currentList
    }
    
    public static func setCurrentList(l: ListTransactions) {
        currentList = l
    }
    
    public static func setStateDoing() {
        state = State.DOING
    }
    
    public static func setStateNone() {
        state = State.NONE;
    }
    
    public static func isDoing() -> Bool {
        return state == State.DOING
    }
    
    public static func getNewID() -> String {
        return UUID().uuidString
    }
}
