//
//  SingleTransactionViewController.swift
//  Smart Wallet
//
//  Created by Mattia Navach on 21/06/2019.
//  Copyright © 2019 Progetto Mobile 2019. All rights reserved.
//

import UIKit

class SingleTransactionViewController: UIViewController {
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelNameShop: UILabel!
    @IBOutlet weak var labelAddressShop: UILabel!
    @IBOutlet weak var labelDateTransaction: UILabel!
    @IBOutlet weak var labelCategory: UILabel!
    @IBOutlet weak var labelAmount: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelName.text = TransactionManager.getName()
        labelNameShop.text = "Nome Negozio: " + TransactionManager.getShopName()
        labelAddressShop.text = "Indirizzo Negozio: " + TransactionManager.getShopAddress()
        labelDateTransaction.text = "Data Transazione: " + TransactionManager.getDate()
        labelCategory.text = "Categoria Transazione: " + TransactionManager.getCategory()
        labelAmount.text = "Totale: " + String(TransactionManager.getAmount().truncate(places: 2))
    }

}
