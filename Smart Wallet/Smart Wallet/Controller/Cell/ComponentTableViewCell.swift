//
//  ComponentTableViewCell.swift
//  Smart Wallet
//
//  Created by Mattia Navach on 24/06/2019.
//  Copyright © 2019 Progetto Mobile 2019. All rights reserved.
//

import UIKit

class ComponentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var labelName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
