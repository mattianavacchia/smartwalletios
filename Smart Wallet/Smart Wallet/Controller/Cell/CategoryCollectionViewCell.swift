//
//  CategoryCollectionViewCell.swift
//  Smart Wallet
//
//  Created by Mattia Navach on 01/06/2019.
//  Copyright © 2019 Progetto Mobile 2019. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var labelNameCategory: UILabel!
    @IBOutlet weak var imageViewCategory: UIImageView!
    
}
