//
//  StatisticsViewController.swift
//  Smart Wallet
//
//  Created by Mattia Navach on 27/06/2019.
//  Copyright © 2019 Progetto Mobile 2019. All rights reserved.
//

import UIKit
import Firebase
import Charts
import MaterialComponents

class StatisticsViewController: UIViewController {
    @IBOutlet weak var incomeLabel: UILabel!
    @IBOutlet weak var expensesLabel: UILabel!
    @IBOutlet weak var pieChart: PieChartView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var showStatistics: UIButton!
    
    var mDatabase: DatabaseReference!
    var userID: String = ""
    private var expenses : Double = 0
    private var income : Double = 0
    private var percExpenses : Double = 0
    private var percIncome : Double = 0
    var totale : [Double] = [0 ,0 ]
    
    let nomi = ["Uscite", "Entrate"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mDatabase = Database.database().reference()
        userID = (Auth.auth().currentUser?.uid)!
        
        showStatistics.layer.borderColor = UIColor.black.cgColor
        showStatistics.layer.borderWidth = 1
        showStatistics.layer.cornerRadius = 5
        showStatistics.clipsToBounds = true
        
        self.pieChart.noDataText = ""
    }
    
    @IBAction func getStatistics(_ sender: Any) {
        if (Reachability.isConnectedToNetwork()) {
            income = 0
            expenses = 0
            let alert = UIAlertController(title: nil, message: "Aggiornamento in corso...", preferredStyle: .alert)
            
            let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
            loadingIndicator.hidesWhenStopped = true
            loadingIndicator.style = UIActivityIndicatorView.Style.gray
            loadingIndicator.startAnimating();
            
            alert.view.addSubview(loadingIndicator)
            self.present(alert, animated: true, completion: nil)
            self.pieChart.clear()
            self.pieChart.noDataText = "Questo mese non ha transazioni"
            incomeLabel.text = "0.00 €"
            expensesLabel.text = "0.00 €"
            loadStatistics()
        } else {
            var message = MDCSnackbarMessage()
            message.text = "Collegare ad una rete Internet per visualizzare i dati."
            MDCSnackbarManager.show(message)
        }
    }
    
    private func loadStatistics(){
        
        mDatabase.child("users").child(userID).child(getMonthYearDatePicker()).observeSingleEvent(of: .value, with: { (snapshot) in
            let enumerator = snapshot.children
            while let rest = enumerator.nextObject() as? DataSnapshot {
                if rest.key != "amount" {
                    self.getSingleTransaction(key: rest.key)
                }
            }
            print("ciao",self.expenses)
            
            self.dismiss(animated: false, completion: nil)
            print("ciao3", self.expenses)
        }) { (error) in
            print(error.localizedDescription)
        }
        print("ciao2",expenses)
        
    }
    
    private func getSingleTransaction(key: String) {
        mDatabase.child("transactions").child(key).observeSingleEvent(of: .value, with: { (snapshot) in
            let value = snapshot.value as? NSDictionary
            
            if value?["type"] as? String ?? "" == "EXPENSES" {
                self.expenses += value?["amount"] as? Double ?? 0.00
            } else {
                self.income += value?["amount"] as? Double ?? 0.00
            }
            let totale = self.expenses + self.income
            self.percExpenses = (self.expenses * 100) / totale
            self.percIncome = (self.income * 100) / totale
            print("entrate", self.percIncome)
            print("spese", self.percExpenses)
            self.totale[0] = self.percExpenses
            self.totale[1] = self.percIncome
            self.incomeLabel.text = String(self.income) + " €"
            self.expensesLabel.text = String(self.expenses) + " €"
            self.customizeChart(dataPoints: self.nomi, values: self.totale.map{ Double($0) })
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    func customizeChart(dataPoints: [String], values: [Double]) {
        
        // 1. Set ChartDataEntry
        var dataEntries: [ChartDataEntry] = []
        for i in 0..<dataPoints.count {
            let dataEntry = PieChartDataEntry(value: values[i], label: values[i] == 0.0 ? "" : dataPoints[i], data: dataPoints[i] as AnyObject)
            dataEntries.append(dataEntry)
        }
        // 2. Set ChartDataSet
        let pieChartDataSet = PieChartDataSet(entries: dataEntries, label: nil)
        pieChartDataSet.colors = colorsOfCharts()
        
    
        // 3. Set ChartData
        let pieChartData = PieChartData(dataSet: pieChartDataSet)
        let format = NumberFormatter()
        format.numberStyle = .percent
        format.maximumFractionDigits = 2
        format.zeroSymbol = ""
        format.multiplier = 1.0
        format.percentSymbol = "%"
        let formatter = DefaultValueFormatter(formatter: format)
        pieChartData.setValueFormatter(formatter)
        // 4. Assign it to the chart’s data
        pieChart.data = pieChartData
        
        let legend = pieChart.legend
        legend.horizontalAlignment = .center
        legend.verticalAlignment = .bottom
        legend.orientation = .horizontal
    }
    
    private func colorsOfCharts() -> [UIColor] {
        var colors: [UIColor] = []
        let red = UIColor.init(named: "colorRed")
        let green = UIColor.init(named: "colorGreen")
        colors.append(red!)
        colors.append(green!)
        return colors
    }
    
    private func getMonthYearDatePicker() -> String {
        datePicker.datePickerMode = UIDatePicker.Mode.date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM yyyy"
        //let monthYearArr = dateFormatter.string(from: datePicker.date).components(separatedBy: " ")
        //let monthYear =
        var monthYear = ""
        let calendar = Calendar.current
        let components = calendar.dateComponents([.day,.month,.year], from: self.datePicker.date)
        if let _ = components.day, let month = components.month, let year = components.year {
            let monthString = Utility.getMonthName(month)
            monthYear = monthString + "-" + String(year)
        }
        return monthYear
    }

}

