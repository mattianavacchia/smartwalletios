//
//  ListsManageViewController.swift
//  Smart Wallet
//
//  Created by Mattia Navach on 23/06/2019.
//  Copyright © 2019 Progetto Mobile 2019. All rights reserved.
//

import UIKit
import Firebase

class ListsManageViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var componentsList = [Component]()
    var mDatabase: DatabaseReference!
    var userID: String = ""

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var tableViewComponents: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelName.text = ListsManager.getCurrentList().getName()
        
        let c = Component()
        c.setId(id: "ID")
        c.setEmail(email: "email")
        componentsList.append(c)
        tableViewComponents.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        mDatabase = Database.database().reference()
        userID = (Auth.auth().currentUser?.uid)!
        
        let alert = UIAlertController(title: nil, message: "Aggiornamento in corso...", preferredStyle: .alert)
        
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
        
        self.dismiss(animated: false, completion: nil)
        
        //getComponents()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return componentsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ComponentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "component_cell") as! ComponentTableViewCell
        cell.labelName.text = componentsList[indexPath.section].getEmail()
        return cell
    }

    @IBAction func addComponent(_ sender: Any) {
    }
    
    @IBAction func deleteList(_ sender: Any) {
    }
    
    @IBAction func changeName(_ sender: Any) {
    }
    
    private func getComponents() {
        componentsList.removeAll()
        
        mDatabase.child("lists").child(ListsManager.getCurrentList().getId()).child("people").observeSingleEvent(of: .value, with: { (snapshot) in
            let enumerator = snapshot.children
            while let rest = enumerator.nextObject() as? DataSnapshot {
                self.getSingleComponent(rest.key)
            }
            
            self.dismiss(animated: false, completion: nil)
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    private func getSingleComponent(_ key: String) {
        mDatabase.child("users").child(key).child("email").observeSingleEvent(of: .value, with: { (snapshot) in
            let c = Component()
            c.setId(id: key)
            c.setEmail(email: (snapshot.value as? String)!)
            
            self.componentsList.append(c)
            self.tableViewComponents.reloadData()
        }) { (error) in
            print(error.localizedDescription)
        }
    }
}
