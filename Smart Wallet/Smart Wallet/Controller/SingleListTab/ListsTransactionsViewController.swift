//
//  ListsTransactionsViewController.swift
//  Smart Wallet
//
//  Created by Mattia Navach on 23/06/2019.
//  Copyright © 2019 Progetto Mobile 2019. All rights reserved.
//

import UIKit
import Firebase

class ListsTransactionsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var transactionsList = [Transaction]()
    var mDatabase: DatabaseReference!
    var userID: String = ""
    
    @IBOutlet weak var tableViewTransactions: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mDatabase = Database.database().reference()
        userID = (Auth.auth().currentUser?.uid)!
    }

    override func viewDidAppear(_ animated: Bool) {
        let alert = UIAlertController(title: nil, message: "Aggiornamento in corso...", preferredStyle: .alert)
        
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
        
        getListTransactions()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let cellSpacingHeight: CGFloat = 5
        return cellSpacingHeight
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transactionsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:SingleTransactionCell = tableView.dequeueReusableCell(withIdentifier: "single_transaction_cell") as! SingleTransactionCell
        cell.labelNameTransaction.text = self.transactionsList[indexPath.section].getName()
        cell.labelDateTransaction.text = self.transactionsList[indexPath.section].getDate()
        cell.labelAmount.text = String(self.transactionsList[indexPath.section].getAmount()) + " €"
        let bgColorView = UIView()
        if self.transactionsList[indexPath.section].getType() == Type.INCOME {
            cell.imageViewCategory.image = UIImage.init(named: CategoryIncome.getResourseFromString(category: self.transactionsList[indexPath.section].getCategory()))
            cell.backgroundColor = UIColor.init(named: "colorGreen")
            bgColorView.backgroundColor = UIColor.init(named: "colorDarkGreen")
            cell.selectedBackgroundView = bgColorView
        } else {
            cell.imageViewCategory.image = UIImage.init(named: CategoryExpenses.getResourseFromString(category: self.transactionsList[indexPath.section].getCategory()))
            cell.backgroundColor = UIColor.init(named: "colorRed")
            bgColorView.backgroundColor = UIColor.init(named: "colorDarkRed")
            cell.selectedBackgroundView = bgColorView
        }
        
        cell.layer.cornerRadius = 8
        cell.clipsToBounds = true
        cell.layer.borderColor = UIColor.black.cgColor
        cell.layer.borderWidth = 1
        return cell
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let closeAction = UIContextualAction(style: .normal, title:  "Modifica", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            self.modify(index: indexPath.section)
            success(true)
        })
        closeAction.backgroundColor = UIColor(named: "colorModify")
        return UISwipeActionsConfiguration(actions: [closeAction])
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let closeAction = UIContextualAction(style: .normal, title:  "Elimina", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            if Reachability.isConnectedToNetwork() {
                // handle delete transaction in list
                self.mDatabase.child("lists").child(ListsManager.getCurrentList().getId())
                    .child(self.transactionsList[indexPath.section].getID())
                    .removeValue()
                self.mDatabase.child("transactions")
                    .child(self.transactionsList[indexPath.section].getID())
                    .removeValue()
                self.transactionsList.remove(at: indexPath.row)
                self.tableViewTransactions.reloadData()
                success(true)
            } else {
            }
        })
        closeAction.backgroundColor = UIColor(named: "colorDelete")
        return UISwipeActionsConfiguration(actions: [closeAction])
    }
    
    private func modify(index: Int) {
        TransactionManager.setCurrentTransaction(t: transactionsList[index])
        TransactionManager.setTypeOperation(op: TypeOperation.MODIFY)
        ListsManager.setStateDoing()
        performSegue(withIdentifier: "transactions_to_modify", sender: self)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        TransactionManager.setCurrentTransaction(t: transactionsList[indexPath.section])
        performSegue(withIdentifier: "transactions_to_single", sender: self)
    }

    
    private func getListTransactions() {
        transactionsList.removeAll()
        mDatabase.child("lists").child(ListsManager.getCurrentList().getId()).observeSingleEvent(of: .value, with: { (snapshot) in
            let enumerator = snapshot.children
            while let rest = enumerator.nextObject() as? DataSnapshot {
                if (rest.key != "name" && rest.key != "people") {
                    self.getSingleTransaction(key: rest.key)
                }
            }
            
            self.dismiss(animated: false, completion: nil)
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    private func getSingleTransaction(key: String) {
        mDatabase.child("transactions").child(key).observeSingleEvent(of: .value, with: { (snapshot) in
            let t = Transaction()
            let value = snapshot.value as? NSDictionary
            // // amount, category, date, description, id, name, pathPhoto, shopAddress, shopName, type
            t.setAmount(amount: value?["amount"] as? Double ?? 0.00)
            t.setName(name: value?["name"] as? String ?? "")
            t.setCategory(category: value?["category"] as? String ?? "")
            t.setDate(date: value?["date"] as? String ?? "")
            t.setDescription(description: value?["description"] as? String ?? "")
            t.setID(id: value?["id"] as? String ?? "")
            t.setPathPhoto(pathPhoto: value?["pathPhoto"] as? String ?? "")
            t.setShopAddress(shopAddress: value?["shopAddress"] as? String ?? "")
            t.setShopName(shopName: value?["shopName"] as? String ?? "")
            if value?["type"] as? String ?? "" == "EXPENSES" {
                t.setType(type: Type.EXPENSES)
            } else {
                t.setType(type: Type.INCOME)
            }
            self.transactionsList.append(t)
            self.tableViewTransactions.reloadData()
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    @IBAction func addTransaction(_ sender: Any) {
        
    }
}
