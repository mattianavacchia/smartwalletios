//
//  DescriptionViewController.swift
//  Smart Wallet
//
//  Created by Mattia Navach on 28/05/2019.
//  Copyright © 2019 Progetto Mobile 2019. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialSnackbar

class DescriptionViewController: UIViewController, UITextViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource {
    
    //let itemsExpenses = ["TAX", "FOOD", "HOLIDAY", "GIFT", "PURCHASE", "FAMILY", "HOBBY", "TRANSPORT", "OTHER"]
    let itemsExpenses = [CategoryExpenses.TAX, CategoryExpenses.FOOD, CategoryExpenses.HOLIDAY, CategoryExpenses.GIFT, CategoryExpenses.PURCHASE, CategoryExpenses.FAMILY, CategoryExpenses.HOBBY, CategoryExpenses.TRANSPORT, CategoryExpenses.OTHER]
    let itemsIncome = [CategoryIncome.GIFT, CategoryIncome.OTHER, CategoryIncome.SALARY, CategoryIncome.RENT, CategoryIncome.REFUND]
    var currentType = Type.EXPENSES

    @IBOutlet weak var buttonExpenses: UIButton!
    @IBOutlet weak var buttonIncome: UIButton!
    @IBOutlet weak var textFieldAmount: UITextField!
    @IBOutlet weak var textFieldName: UITextField!
    @IBOutlet weak var textFieldShopName: UITextField!
    @IBOutlet weak var textFieldShopAddress: UITextField!
    @IBOutlet weak var labelCurrency: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var textFieldDescription: UITextView!
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonIncome.layer.cornerRadius = 10
        buttonIncome.clipsToBounds = true
        buttonExpenses.layer.cornerRadius = 10
        buttonExpenses.clipsToBounds = true
        self.hideKeyboard()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        loadDataIntoComponents()
    }
    
    private func loadDataIntoComponents()  {
        textFieldAmount.text = String(TransactionManager.getAmount())
        textFieldShopName.text = TransactionManager.getShopName()
        textFieldName.text = TransactionManager.getName()
        textFieldShopAddress.text = TransactionManager.getShopAddress()
        textFieldDescription.text = TransactionManager.getDescription()
        currentType = Type(rawValue: TransactionManager.getType())!
        if (currentType == Type.EXPENSES) {
            changeTypeToExpenses()
        } else {
            changeTypeToIncome()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if currentType == Type.EXPENSES {
            return itemsExpenses.count
        } else {
            return itemsIncome.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellCategory", for: indexPath) as! CategoryCollectionViewCell
        
        if currentType == Type.EXPENSES {
            cell.labelNameCategory.text = CategoryExpenses.getName(category: itemsExpenses[indexPath.item])
            cell.imageViewCategory.backgroundColor = UIColor.init(named: "colorRed")
            cell.imageViewCategory.image = UIImage(named: CategoryExpenses.getResourse(category: itemsExpenses[indexPath.item]))
            cell.backgroundColor = UIColor.init(named: "colorRed")
        } else {
            cell.labelNameCategory.text = CategoryIncome.getName(category: itemsIncome[indexPath.item])
            cell.imageViewCategory.backgroundColor = UIColor.init(named: "colorGreen")
            cell.imageViewCategory.image = UIImage(named: CategoryIncome.getResourse(category: itemsIncome[indexPath.item]))
            cell.backgroundColor = UIColor.init(named: "colorGreen")
        }
        
        cell.layer.cornerRadius = 20
        cell.clipsToBounds = true
        cell.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tap(_:))))
        
        return cell
    }
    
    @objc func tap(_ sender: UITapGestureRecognizer) {
        
        let location = sender.location(in: self.categoryCollectionView)
        let indexPath = self.categoryCollectionView.indexPathForItem(at: location)
        
        let message = MDCSnackbarMessage()
        if let index = indexPath {
            if currentType == Type.EXPENSES {
                TransactionManager.setCategory(category: CategoryExpenses.getName(category: itemsExpenses[indexPath!.item]))
                message.text = "Aggiunta la categoria \(itemsExpenses[index.item]) alla Transazione."
            } else {
                TransactionManager.setCategory(category: CategoryIncome.getName(category: itemsIncome[indexPath!.item]))
                message.text = "Aggiunta la categoria \(itemsIncome[index.item]) alla Transazione."
            }
            //print("Got clicked on index: \(index.item)!")
            MDCSnackbarManager.show(message)
        }
    }
    
    @IBAction func clickChangeTypeToExpenses(_ sender: UIButton) {
        self.changeTypeToExpenses()
    }
    
    private func changeTypeToExpenses() {
        textFieldAmount.textColor = UIColor(named: "colorRed")
        labelCurrency.textColor = UIColor(named: "colorRed")
        //categoryCollectionView.backgroundColor = UIColor(named: "colorRed")
        self.currentType = Type.EXPENSES
        TransactionManager.setType(type: Type.EXPENSES)
        self.categoryCollectionView.reloadData()
    }
    
    @IBAction func clickChangeTypeToIncome(_ sender: Any) {
        self.changeTypeToIncome()
    }
    
    private func changeTypeToIncome() {
        textFieldAmount.textColor = UIColor(named: "colorGreen")
        labelCurrency.textColor = UIColor(named: "colorGreen")
        //categoryCollectionView.backgroundColor = UIColor(named: "colorGreen")
        self.currentType = Type.INCOME
        TransactionManager.setType(type: Type.INCOME)
        self.categoryCollectionView.reloadData()
    }
    
    @IBAction func endEditingAmount(_ sender: Any) {
        TransactionManager.setAmount(amount: Double(textFieldAmount.text!)!)
        datePicker.datePickerMode = UIDatePicker.Mode.date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM yyyy"
        TransactionManager.setDate(date: dateFormatter.string(from: datePicker.date))
    }
    
    @IBAction func endEditingName(_ sender: Any) {
        TransactionManager.setName(name: textFieldName.text!)
    }
    
    @IBAction func endEditingShopName(_ sender: Any) {
        TransactionManager.setShopName(shopName: textFieldShopName.text!)
    }
    
    @IBAction func endEditingShopAddress(_ sender: Any) {
        TransactionManager.setShopAddress(shopAddress: textFieldShopAddress.text!)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        TransactionManager.setDescription(description: textFieldDescription.text!)
    }
    
}
