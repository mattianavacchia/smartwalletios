//
//  EndViewController.swift
//  Smart Wallet
//
//  Created by Mattia Navach on 30/05/2019.
//  Copyright © 2019 Progetto Mobile 2019. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class EndViewController: UIViewController {
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelShopName: UILabel!
    @IBOutlet weak var labelShopAddress: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelCategory: UILabel!
    @IBOutlet weak var labelAmount: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        loadContent()
    }
    
    private func loadContent() {
        labelName.text = TransactionManager.getName()
        labelShopName.text = "Nome Negozio: " + TransactionManager.getShopName()
        labelShopAddress.text = "Indirizzo Negozio: " + TransactionManager.getShopAddress()
        labelDate.text = "Data Transazione: " + TransactionManager.getDate()
        labelCategory.text = "Categoria Transazione: " + TransactionManager.getCategory()
        labelAmount.text = "Totale: " + String(TransactionManager.getAmount())
    }
    
    private func clearAndGo() {
        TransactionManager.clearTransaction()
        self.performSegue(withIdentifier: "end_to_main", sender: nil)
    }
    
    @IBAction func saveTransaction(_ sender: Any) {
        if TransactionManager.checkImportantFields() && Reachability.isConnectedToNetwork() {
            var mDatabase: DatabaseReference!
            mDatabase = Database.database().reference()
            
            if Reachability.isConnectedToNetwork() {
                let transaction = ["amount": TransactionManager.getAmount(),
                                   "category": TransactionManager.getCategory(),
                                   "date": TransactionManager.getDate(),
                                   "description": TransactionManager.getDescription(),
                                   "id": TransactionManager.getID(),
                                   "name": TransactionManager.getName(),
                                   "pathPhoto": TransactionManager.getPathPhoto(),
                                   "shopAddress": TransactionManager.getShopAddress(),
                                   "shopName": TransactionManager.getShopName(),
                                   "type": TransactionManager.getType()] as [String : Any]
                if !ListsManager.isDoing() {
                    let d_split = TransactionManager.getDate().split(separator: " ")
                    let date_id_string = Utility.getMonthNameItalia(String(d_split[1])) + "-" + d_split[2]
                    mDatabase.child("users").child(Auth.auth().currentUser!.uid).child(Utility.getMonthYear()).child(TransactionManager.getID()).setValue(true)
                    
                      if TransactionManager.hasChanged() {
                        var x = TransactionManager.getAmountOfTheMonth()
                        if (TransactionManager.getAmountBeforeCurrent() > 0.00) {
                            if (TransactionManager.getType() == Type.INCOME.rawValue) {
                                x = TransactionManager.getAmountOfTheMonth() - TransactionManager.getAmountBeforeCurrent()
                            } else {
                                x = TransactionManager.getAmountOfTheMonth() + TransactionManager.getAmountBeforeCurrent()
                            }
                        }
                        x = TransactionManager.getType() == Type.INCOME.rawValue ? x + TransactionManager.getAmount() : x - TransactionManager.getAmount();
                        mDatabase.child("users").child(Auth.auth().currentUser!.uid).child(Utility.getMonthYear()).child("amount").setValue(x);
                    }
                } else {
                    mDatabase.child("lists").child(ListsManager.getCurrentList().getId()).child("transactions").child(TransactionManager.getID()).setValue(true)
                }
                // amount, category, date, description, id, name, pathPhoto, shopAddress, shopName, type
                mDatabase.child("transactions").child(TransactionManager.getID()).setValue(transaction)
                
                let alert = UIAlertController(title: "Aggiunta o Modificata Transazione.", message: "Transazione aggiunta con successo.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    self.clearAndGo()
                }))
                self.present(alert, animated: true)
            } else {
                let alert = UIAlertController(title: "Errore di connessione.", message: "Attiva la tua connessione dati per effettuare il salvataggio.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true)
            }
        } else {
            let alert = UIAlertController(title: "Campi Errati o Mancanza di Connessione.", message: "Non sono stati compilati tutti i campi minimi (Totale, Categoria, Data, Tipo)", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    private func cancel(_ sender: UIButton) {
        self.clearAndGo()
    }
}
