//
//  PhotoViewController.swift
//  Smart Wallet
//
//  Created by Mattia Navach on 28/05/2019.
//  Copyright © 2019 Progetto Mobile 2019. All rights reserved.
//

import UIKit

class PhotoViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    static var imageSelected: UIImage?
    var imagePicker: UIImagePickerController!
    @IBOutlet weak var imageViewSelected: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageViewSelected.image = PhotoViewController.imageSelected
    }
    
    public static func setImageViewSelected(_ image: UIImage) {
        imageSelected = image
    }
    
    @IBAction func takeFromLibrary(_ sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) {
            imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func takeFromCamera(_ sender: Any) {
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: { () -> Void in
        })
        imagePicker.dismiss(animated: true, completion: nil)
        imageViewSelected.image = info[.originalImage] as? UIImage
    }
}
