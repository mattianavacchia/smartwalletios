//
//  TransactionsViewController.swift
//  Smart Wallet
//
//  Created by Mattia Navach on 18/06/2019.
//  Copyright © 2019 Progetto Mobile 2019. All rights reserved.
//

import UIKit
import Firebase
import MaterialComponents.MaterialSnackbar

class TransactionsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var tableViewTransactions: UITableView!
    @IBOutlet weak var buttonShowTransactions: UIButton!
    @IBOutlet weak var buttonToStatistics: UIButton!
    @IBOutlet weak var buttonToMap: UIButton!
    
    var mDatabase: DatabaseReference!
    var userID: String = ""
    var transactionsList = [Transaction]()
    let cellSpacingHeight: CGFloat = 5
    var message = MDCSnackbarMessage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mDatabase = Database.database().reference()
        userID = (Auth.auth().currentUser?.uid)!
        
        buttonShowTransactions.layer.borderColor = UIColor.black.cgColor
        buttonShowTransactions.layer.borderWidth = 1
        buttonShowTransactions.layer.cornerRadius = 5
        buttonShowTransactions.clipsToBounds = true
        buttonToStatistics.layer.cornerRadius = 5
        buttonToStatistics.clipsToBounds = true
        buttonToMap.layer.cornerRadius = 5
        buttonToMap.clipsToBounds = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.transactionsList.count
    }
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        TransactionManager.setCurrentTransaction(t: transactionsList[indexPath.section])
        performSegue(withIdentifier: "transactions_to_single", sender: self)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:SingleTransactionCell = tableView.dequeueReusableCell(withIdentifier: "single_transaction_cell") as! SingleTransactionCell
        cell.labelNameTransaction.text = self.transactionsList[indexPath.section].getName()
        cell.labelDateTransaction.text = self.transactionsList[indexPath.section].getDate()
        cell.labelAmount.text = String(self.transactionsList[indexPath.section].getAmount()) + " €"
        let bgColorView = UIView()
        if self.transactionsList[indexPath.section].getType() == Type.INCOME {
            cell.imageViewCategory.image = UIImage.init(named: CategoryIncome.getResourseFromString(category: self.transactionsList[indexPath.section].getCategory()))
            cell.backgroundColor = UIColor.init(named: "colorGreen")
            bgColorView.backgroundColor = UIColor.init(named: "colorDarkGreen")
            cell.selectedBackgroundView = bgColorView
        } else {
            cell.imageViewCategory.image = UIImage.init(named: CategoryExpenses.getResourseFromString(category: self.transactionsList[indexPath.section].getCategory()))
            cell.backgroundColor = UIColor.init(named: "colorRed")
            bgColorView.backgroundColor = UIColor.init(named: "colorDarkRed")
            cell.selectedBackgroundView = bgColorView
        }
        
        cell.layer.cornerRadius = 8
        cell.clipsToBounds = true
        cell.layer.borderColor = UIColor.black.cgColor
        cell.layer.borderWidth = 1
        return cell
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let closeAction = UIContextualAction(style: .normal, title:  "Modifica", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            self.modify(index: indexPath.section)
            success(true)
        })
        closeAction.backgroundColor = UIColor(named: "colorModify")
        return UISwipeActionsConfiguration(actions: [closeAction])
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let closeAction = UIContextualAction(style: .normal, title:  "Elimina", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            if Reachability.isConnectedToNetwork() {
                // handle new amount
                var amount = TransactionManager.getAmountOfTheMonth()
                if self.transactionsList[indexPath.section].getType() == Type.EXPENSES {
                    amount += self.transactionsList[indexPath.section].getAmount()
                } else {
                    amount -= self.transactionsList[indexPath.section].getAmount()
                }
                self.mDatabase.child("users").child(Auth.auth().currentUser!.uid)
                    .child(self.getMonthYearDatePicker())
                    .child("amount")
                    .setValue(amount)
                // remove from transactions
                self.mDatabase.child("transactions").child(self.transactionsList[indexPath.section].getID()).removeValue()
                // remove from user
                self.mDatabase.child("users").child(Auth.auth().currentUser!.uid)
                    .child(self.getMonthYearDatePicker())
                    .child(self.transactionsList[indexPath.section].getID())
                    .removeValue()
                self.transactionsList.remove(at: indexPath.row)
                self.tableViewTransactions.reloadData()
                success(true)
            } else {
                let message = MDCSnackbarMessage()
                message.text = "Impossibile eseguire operazione senza rete."
                MDCSnackbarManager.show(message)
            }
        })
        closeAction.backgroundColor = UIColor(named: "colorDelete")
        return UISwipeActionsConfiguration(actions: [closeAction])
    }
    
    private func modify(index: Int) {
        TransactionManager.setCurrentTransaction(t: transactionsList[index])
        TransactionManager.setTypeOperation(op: TypeOperation.MODIFY)
        performSegue(withIdentifier: "transactions_to_modify", sender: self)
    }

    @IBAction func showTransactions(_ sender: Any) {
        if (Reachability.isConnectedToNetwork()) {
            // please wait popup
            let alert = UIAlertController(title: nil, message: "Aggiornamento in corso...", preferredStyle: .alert)
            
            let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
            loadingIndicator.hidesWhenStopped = true
            loadingIndicator.style = UIActivityIndicatorView.Style.gray
            loadingIndicator.startAnimating();
            
            alert.view.addSubview(loadingIndicator)
            self.present(alert, animated: true, completion: nil)
            
            transactionsList.removeAll()
            self.tableViewTransactions.reloadData()
            getTransactions()
            
        } else {
            message.text = "Collegare ad una rete Internet per visualizzare i dati."
            MDCSnackbarManager.show(message)
        }
    }
    
    @IBAction func goToStatistics(_ sender: Any) {
        performSegue(withIdentifier: "transactions_to_statistics", sender: self)
    }
    
    @IBAction func goTomap(_ sender: Any) {
        if (transactionsList.count > 0) {
            performSegue(withIdentifier: "transactions_to_map", sender: self)
        } else {
            message.text = "Carica le transazioni desiderate prima di visualizzare la mappa."
            MDCSnackbarManager.show(message)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "transactions_to_map" {
            if let viewController = segue.destination as? MapViewController {
                viewController.transactionsList = self.transactionsList
            }
        }
    }
    
    private func getMonthYearDatePicker() -> String {
        datePicker.datePickerMode = UIDatePicker.Mode.date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM yyyy"
        //let monthYearArr = dateFormatter.string(from: datePicker.date).components(separatedBy: " ")
        //let monthYear =
        var monthYear = ""
        let calendar = Calendar.current
        let components = calendar.dateComponents([.day,.month,.year], from: self.datePicker.date)
        if let _ = components.day, let month = components.month, let year = components.year {
            let monthString = Utility.getMonthName(month)
            monthYear = monthString + "-" + String(year)
        }
        return monthYear
    }
    
    private func getTransactions() {
        
        mDatabase.child("users").child(userID).child(getMonthYearDatePicker()).observeSingleEvent(of: .value, with: { (snapshot) in
            let enumerator = snapshot.children
            while let rest = enumerator.nextObject() as? DataSnapshot {
                if rest.key != "amount" {
                    self.getSingleTransaction(key: rest.key)
                }
            }
            
            self.dismiss(animated: false, completion: nil)
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    private func getSingleTransaction(key: String) {
        mDatabase.child("transactions").child(key).observeSingleEvent(of: .value, with: { (snapshot) in
            let t = Transaction()
            let value = snapshot.value as? NSDictionary
            // // amount, category, date, description, id, name, pathPhoto, shopAddress, shopName, type
            t.setAmount(amount: value?["amount"] as? Double ?? 0.00)
            t.setName(name: value?["name"] as? String ?? "")
            t.setCategory(category: value?["category"] as? String ?? "")
            t.setDate(date: value?["date"] as? String ?? "")
            t.setDescription(description: value?["description"] as? String ?? "")
            t.setID(id: value?["id"] as? String ?? "")
            t.setPathPhoto(pathPhoto: value?["pathPhoto"] as? String ?? "")
            t.setShopAddress(shopAddress: value?["shopAddress"] as? String ?? "")
            t.setShopName(shopName: value?["shopName"] as? String ?? "")
            if value?["type"] as? String ?? "" == "EXPENSES" {
                t.setType(type: Type.EXPENSES)
            } else {
                t.setType(type: Type.INCOME)
            }
            self.transactionsList.append(t)
            self.tableViewTransactions.reloadData()
        }) { (error) in
            print(error.localizedDescription)
        }
    }
}
