//
//  SignInViewController.swift
//  Smart Wallet
//
//  Created by Elia Pasqualini on 13/05/2019.
//  Copyright © 2019 Progetto Mobile 2019. All rights reserved.
//

import UIKit
import Firebase
import MaterialComponents

class SignInViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Check if the user is already Logged
        if !Reachability.isConnectedToNetwork(){
            var message = MDCSnackbarMessage()
            message.text = "Collegare ad una rete Internet per visualizzare i dati."
            MDCSnackbarManager.show(message)
        } else {
            Auth.auth().addStateDidChangeListener { auth, user in
                if user != nil {
                    self.performSegue(withIdentifier: "login_main", sender: self)
                }
            }
        }
        
        /*self.navigationController?.navigationItem.backBarButtonItem?.isEnabled = false*/
        //self.navigationController?.navigationItem.hidesBackButton = true
        self.navigationController?.isNavigationBarHidden = true
        self.hideKeyboard()
    }

    @IBAction func login(_ sender: UIButton) {
        let email = String(txt_email.text!)
        let password = String(txt_password.text!)
        
        Auth.auth().signIn(withEmail: email, password: password) { [weak self] user, error in
            guard self != nil else { return }
            
            if error != nil {
                
                print(error.debugDescription)
                
                let alert = UIAlertController(title: "Errore", message: Utility.messageErrorFromCode(code: error!._code), preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                
                self?.present(alert, animated: true)
                
            }else{
                //user registered successfully
                let alert = UIAlertController(title: "Errore", message: "Login effettuato con successo.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                
                self?.performSegue(withIdentifier: "login_main", sender: self)
            }
            
        }
    }
    
    @IBAction func goToRegistration(_ sender: UIButton) {
        self.performSegue(withIdentifier: "login_registration", sender: self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}

extension UIViewController {
    func hideKeyboard() {
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        
        view.addGestureRecognizer(tap)
    }
    
    //Calls this function when the tap is recognized.
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
}
