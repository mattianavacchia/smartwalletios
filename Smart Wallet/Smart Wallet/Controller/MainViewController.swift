//
//  MainViewController.swift
//  Smart Wallet
//
//  Created by Elia Pasqualini on 13/05/2019.
//  Copyright © 2019 Progetto Mobile 2019. All rights reserved.
//

import UIKit
import Firebase
import MaterialComponents

class MainViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var transactionButton: UIButton!
    
    var imageView = UIImageView()
    var imageUrl: String = ""
    var imagePicker: UIImagePickerController!
    var menuIsHidden = true
    let controller = "MainViewController"
    var mDatabase: DatabaseReference!
    var userID: String = ""
    var transactionsList = [Transaction]()
    let cellSpacingHeight: CGFloat = 5
    var message = MDCSnackbarMessage()
    
    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewAmountOfTheMonth: UIView!
    @IBOutlet weak var labelAmountOfTheMonth: UILabel!
    @IBOutlet weak var labelCurrentMonth: UILabel!
    @IBOutlet weak var tableViewLastTransactions: UITableView!
    
    
    var actionButton : ActionButton!
    
    fileprivate var blurVisualEffect: UIVisualEffectView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // set up menu
        leadingConstraint.constant = -190
        
        // add cornes to view of total amount
        viewAmountOfTheMonth.layer.cornerRadius = 20
        viewAmountOfTheMonth.clipsToBounds = true
        viewAmountOfTheMonth.layer.borderColor = UIColor.black.cgColor
        viewAmountOfTheMonth.layer.borderWidth = 1
        
        // change manu blur and corners
        menuView.clipsToBounds = true
        menuView.layer.cornerRadius = 10
        menuView.layer.borderWidth = 1
        menuView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        
        self.blurVisualEffect = UIVisualEffectView(effect: UIBlurEffect(style: .extraLight))
        self.blurVisualEffect.removeFromSuperview()
        
        // set current month
        labelCurrentMonth.text = "Mese di " + Utility.getCurrentMonth()
        
        if (Reachability.isConnectedToNetwork()) {
            mDatabase = Database.database().reference()
            userID = (Auth.auth().currentUser?.uid)!
            
            transactionsList.removeAll()
            tableViewLastTransactions.reloadData()
            
            getAmountOfTheMonth()
            getTransactions()
        } else {
            message.text = "Collegare ad una rete Internet per visualizzare i dati."
            MDCSnackbarManager.show(message)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        // floating button
        setupFloatingAddButton();
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.transactionsList.count
    }

    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        TransactionManager.setCurrentTransaction(t: transactionsList[indexPath.section])
        performSegue(withIdentifier: "main_to_single", sender: self)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:SingleTransactionCell = tableView.dequeueReusableCell(withIdentifier: "single_transaction_cell") as! SingleTransactionCell
        cell.labelNameTransaction.text = self.transactionsList[indexPath.section].getName()
        cell.labelDateTransaction.text = self.transactionsList[indexPath.section].getDate()
        cell.labelAmount.text = String(self.transactionsList[indexPath.section].getAmount()) + " €"
        if self.transactionsList[indexPath.section].getType() == Type.INCOME {
            cell.imageViewCategory.image = UIImage.init(named: CategoryIncome.getResourseFromString(category: self.transactionsList[indexPath.section].getCategory()))
            cell.backgroundColor = UIColor.init(named: "colorGreen")
        } else {
            cell.imageViewCategory.image = UIImage.init(named: CategoryExpenses.getResourseFromString(category: self.transactionsList[indexPath.section].getCategory()))
            
            cell.backgroundColor = UIColor.init(named: "colorRed")
        }
        
        cell.layer.cornerRadius = 8
        cell.clipsToBounds = true
        cell.layer.borderColor = UIColor.black.cgColor
        cell.layer.borderWidth = 1
        
        
        return cell
    }
    
    private func getAmountOfTheMonth() {

        if (Reachability.isConnectedToNetwork()) {
            mDatabase.child("users").child(userID).child(Utility.getMonthYear()).child("amount").observeSingleEvent(of: .value, with: { (snapshot) in
                let value = snapshot.value as? Double
                if(value == nil) {
                    self.labelAmountOfTheMonth.text = "0"
                    self.viewAmountOfTheMonth.backgroundColor = UIColor.init(named: "colorGreen")
                } else {
                    self.labelAmountOfTheMonth.text = String(value!.truncate(places: 2))
                    if value!.truncate(places: 2) > 0.00 {
                        self.viewAmountOfTheMonth.backgroundColor = UIColor.init(named: "colorGreen")
                    } else {
                        self.viewAmountOfTheMonth.backgroundColor = UIColor.init(named: "colorRed")
                    }
                    TransactionManager.setAmountOfTheMonth(amount: value!.truncate(places: 2))
                }
                
            }) { (error) in
                print(error.localizedDescription)
            }
        } else {
            message.text = "Collegare ad una rete Internet per visualizzare i dati."
            MDCSnackbarManager.show(message)
        }
    }
    
    private func getTransactions() {
        // please wait popup
        if (Reachability.isConnectedToNetwork()) {
            /*let alert = UIAlertController(title: nil, message: "Aggiornamento in corso...", preferredStyle: .alert)
            
            let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
            loadingIndicator.hidesWhenStopped = true
            loadingIndicator.style = UIActivityIndicatorView.Style.gray
            loadingIndicator.startAnimating();
            
            alert.view.addSubview(loadingIndicator)
            self.present(alert, animated: true, completion: nil)*/
            
            transactionsList.removeAll()
            mDatabase.child("users").child(userID).child(Utility.getMonthYear()).observeSingleEvent(of: .value, with: { (snapshot) in
                let enumerator = snapshot.children
                if !snapshot.hasChildren() {
                    self.tableViewLastTransactions.reloadData()
                } else {
                    while let rest = enumerator.nextObject() as? DataSnapshot {
                        if rest.key != "amount" {
                            self.getSingleTransaction(key: rest.key)
                        }
                    }
                }
                
                //self.dismiss(animated: false, completion: nil)
            }) { (error) in
                print(error.localizedDescription)
            }
        } else {
            message.text = "Collegare ad una rete Internet per visualizzare i dati."
            MDCSnackbarManager.show(message)
        }
    }
    
    private func getSingleTransaction(key: String) {
        mDatabase.child("transactions").child(key).observeSingleEvent(of: .value, with: { (snapshot) in
            let t = Transaction()
            let value = snapshot.value as? NSDictionary
            // // amount, category, date, description, id, name, pathPhoto, shopAddress, shopName, type
            t.setAmount(amount: value?["amount"] as? Double ?? 0.00)
            t.setName(name: value?["name"] as? String ?? "")
            t.setCategory(category: value?["category"] as? String ?? "")
            t.setDate(date: value?["date"] as? String ?? "")
            t.setDescription(description: value?["description"] as? String ?? "")
            t.setID(id: value?["id"] as? String ?? "")
            t.setPathPhoto(pathPhoto: value?["pathPhoto"] as? String ?? "")
            t.setShopAddress(shopAddress: value?["shopAddress"] as? String ?? "")
            t.setShopName(shopName: value?["shopName"] as? String ?? "")
            if value?["type"] as? String ?? "" == "EXPENSES" {
                t.setType(type: Type.EXPENSES)
            } else {
                t.setType(type: Type.INCOME)
            }
            self.transactionsList.append(t)
            self.tableViewLastTransactions.reloadData()
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    @IBAction func toggleMenu(_ sender: Any) {
        self.blurVisualEffect.frame = view.frame
        view.insertSubview(self.blurVisualEffect, at: 4)
        if menuIsHidden {
            leadingConstraint.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        } else {
            blurVisualEffect.removeFromSuperview()
            leadingConstraint.constant = -190
            
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
        if actionButton.active {
            actionButton.toggleMenu()
        }
        menuIsHidden = !menuIsHidden
    }
    
    @IBAction func goToAllTransactions(_ sender: Any) {
        self.blurVisualEffect.removeFromSuperview()
        toggleMenu(sender)
        performSegue(withIdentifier: "main_to_all_transactions", sender: self)
    }
    
    @IBAction func goToTransactions(_ sender: Any) {
        self.blurVisualEffect.removeFromSuperview()
        performSegue(withIdentifier: "main_to_all_transactions", sender: self)
    }
    
    @IBAction func goToStatistics(_ sender: Any) {
        self.blurVisualEffect.removeFromSuperview()
        //bisognerebbe anche chiudere il menù
        toggleMenu(sender)
        performSegue(withIdentifier: "main_to_statistics", sender: self)
        
    }
    
    @IBAction func goToLists(_ sender: Any) {
        self.blurVisualEffect.removeFromSuperview()
        toggleMenu(sender)
        performSegue(withIdentifier: "main_to_lists", sender: self)
    }
    
    @IBAction func goToSettings(_ sender: Any) {
        self.blurVisualEffect.removeFromSuperview()
        toggleMenu(sender)
        performSegue(withIdentifier: "main_to_settings", sender: self)
    }
    
    private func setupFloatingAddButton() {
        let addByHand = ActionButtonItem(title: "Aggiungi manualmente", image: #imageLiteral(resourceName: "Hand Floating"))
        addByHand.action = { item in
            self.performeSegue(name: "main_add_transaction", TypeOperation.EMPTY)
            self.actionButton.toggleMenu()
        }
        let addWithOCR = ActionButtonItem(title: "Aggiungi con OCR", image: #imageLiteral(resourceName: "OCR Floating"))
        addWithOCR.action = { item in
            //self.performeSegue(name: "main_take_photo", TypeOperation.OCR)
            // create the alert
            let alert = UIAlertController(title: "Seleziona l'immagine!", message: "Assicurati che l'immagine sia di buona qualità", preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "Scegli dalla galleria", style: UIAlertAction.Style.default, handler: { action in
                self.openGallery()
                self.actionButton.toggleMenu()
            }))
            alert.addAction(UIAlertAction(title: "Scatta foto", style: UIAlertAction.Style.default, handler: { action in
                self.openCamera()
                self.actionButton.toggleMenu()
            }))
            alert.addAction(UIAlertAction(title: "Annulla", style: UIAlertAction.Style.cancel, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
            
        }
        actionButton = ActionButton(attachedToView: self.view, items: [addByHand, addWithOCR])
        actionButton.setTitle("+", forState: UIControl.State())
        actionButton.backgroundColor = UIColor(red: 165.0/255.0, green: 165.0/255.0, blue: 165.0/255.0, alpha: 1)
        actionButton.action = { button in button.toggleMenu()}
    }
    
    private func performeSegue(name: String, _ op: TypeOperation) {
        if (op != TypeOperation.MODIFY) {
            TransactionManager.initializeTransaction()
        }
        TransactionManager.setTypeOperation(op: op)
        performSegue(withIdentifier: name, sender: self)
    }
    
    @IBAction func logout(_ sender: UIButton) {
        //print("logout")
        self.blurVisualEffect.removeFromSuperview()
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
            performSegue(withIdentifier: "logout", sender: self)
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
        
    }
    
    func openCamera() {
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    func openGallery() {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) {
            imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "main_add_transaction" {
            let vc = segue.destination as! TabViewController
            if TransactionManager.getTypeOperation() != TypeOperation.EMPTY {
                vc.imageSelected = imageView.image
                PhotoViewController.setImageViewSelected(imageView.image!)
            }
        }
    }
    
    /*func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: { () -> Void in
        })
        imagePicker.dismiss(animated: true, completion: nil)
        imageView.image = info[.originalImage] as? UIImage
        if info[.imageURL] as? String != nil {
            imageUrl = (info[.imageURL] as? String)!
        }
        performeSegue(name: "main_add_transaction", TypeOperation.OCR)
    }*/
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: { () -> Void in
        })
        imagePicker.dismiss(animated: true, completion: nil)
        //obtaining saving path
        let fileManager = FileManager.default
        let documentsPath = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first
        imageUrl = (documentsPath?.appendingPathComponent("image.jpg").path)!
        print("URL" , imageUrl)
        
        imageView.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        
        performeSegue(name: "main_add_transaction", TypeOperation.OCR)
    }
    
}

extension Double {
    func truncate(places : Int)-> Double {
        return Double(floor(pow(10.0, Double(places)) * self)/pow(10.0, Double(places)))
    }
}
