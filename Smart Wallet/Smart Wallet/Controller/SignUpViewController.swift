//
//  SignUpViewController.swift
//  Smart Wallet
//
//  Created by Mattia Navach on 09/05/2019.
//  Copyright © 2019 Progetto Mobile 2019. All rights reserved.
//

import UIKit
import Firebase
import MaterialComponents

class SignUpViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*self.navigationController?.navigationItem.backBarButtonItem?.isEnabled = false*/
        
        //self.navigationController?.navigationItem.hidesBackButton = true
        self.navigationController?.isNavigationBarHidden = true
        
        self.hideKeyboard()
    }
    
    @IBAction func registration(_ sender: UIButton) {
        if (Reachability.isConnectedToNetwork()) {
            let email = String(txt_email.text!)
            let password = String(txt_password.text!)
            
            Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
                
                if error != nil {
                    let alert = UIAlertController(title: "Errore", message: Utility.messageErrorFromCode(code: error!._code), preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    
                    self.present(alert, animated: true)
                }else{
                    //user registered successfully
                    let alert = UIAlertController(title: "Registrazione", message: "Registrazione effettuata con successo. Effettua il Login.", preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        //run your function here
                        self.performSegue(withIdentifier: "registration_login", sender: self)
                    }))
                    self.present(alert, animated: true)
                }
            }
        } else {
            var message = MDCSnackbarMessage()
            message.text = "Collegare ad una rete Internet per visualizzare i dati."
            MDCSnackbarManager.show(message)
        }
    }

    @IBAction func registrationToLogin(_ sender: UIButton) {
        performSegue(withIdentifier: "registration_login", sender: self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
