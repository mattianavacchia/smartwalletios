//
//  MapViewController.swift
//  Smart Wallet
//
//  Created by Mattia Navach on 27/06/2019.
//  Copyright © 2019 Progetto Mobile 2019. All rights reserved.
//

import UIKit
import GoogleMaps

class MapViewController: UIViewController {
    
    @IBOutlet weak var mapView: GMSMapView!
    public var transactionsList = [Transaction]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.layoutIfNeeded()
        self.navigationItem.backBarButtonItem?.title = "Indietro"
        // 43.380686, 12.153608
        let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: 43.380686, longitude: 12.153608, zoom: 8.0)
        mapView.camera = camera
        
        for var t in transactionsList {
            let address = t.getshopAddress()
            
            let geoCoder = CLGeocoder()
            geoCoder.geocodeAddressString(address) { (placemarks, error) in
                guard
                    let placemarks = placemarks,
                    let location = placemarks.first?.location
                    else {
                        // handle no location found
                        return
                }
                
                self.setMarker((location.coordinate.latitude), (location.coordinate.longitude), t.getshopName())
            }
        }
    }
    
    private func setMarker(_ lat: Double, _ lon: Double, _ title: String) {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        marker.title = title
        marker.map = mapView
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
