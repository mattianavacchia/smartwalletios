//
//  SettingsViewController.swift
//  Smart Wallet
//
//  Created by Mattia Navach on 27/06/2019.
//  Copyright © 2019 Progetto Mobile 2019. All rights reserved.
//

import UIKit
import Firebase
import MaterialComponents.MaterialSnackbar

class SettingsViewController: UIViewController {

    var mDatabase: DatabaseReference!
    var userID: String = ""
    
    @IBOutlet weak var buttonSave: UIButton!
    @IBOutlet weak var buttonDelete: UIButton!
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mDatabase = Database.database().reference()
        userID = (Auth.auth().currentUser?.uid)!
        
        buttonSave.clipsToBounds = true
        buttonSave.layer.cornerRadius = 5
        buttonSave.layer.borderColor = UIColor.black.cgColor
        buttonSave.layer.borderWidth = 1
        buttonDelete.clipsToBounds = true
        buttonDelete.layer.cornerRadius = 5
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func saveSettings(_ sender: Any) {
        if (Reachability.isConnectedToNetwork()) {
            var message = MDCSnackbarMessage()
            if (!(textFieldPassword.text?.isEmpty)!) {
                let password = textFieldPassword.text!
                Auth.auth().currentUser?.updatePassword(to: password) { (error) in
                    if error != nil{
                        message.text = "Password aggiornata con successo."
                    }else{
                        message.text = "Errore durante la modifica della password. Riprovare."
                    }
                }
            } else {
                if (!(textFieldEmail.text?.isEmpty)!) {
                    let email = textFieldEmail.text!
                    Auth.auth().currentUser?.updateEmail(to: email) { (error) in
                        let message = MDCSnackbarMessage()
                        if error != nil{
                            message.text = "Email aggiornata con successo."
                        }else{
                            message.text = "Errore durante la modifica della email. Riprovare."
                        }
                    }
                } else {
                    message.text = "Compilare almeno uno dei campi prima di apportare modifiche."
                }
            }
            
            MDCSnackbarManager.show(message)
        } else {
            var message = MDCSnackbarMessage()
            message.text = "Collegare ad una rete Internet per visualizzare i dati."
            MDCSnackbarManager.show(message)
        }
    }
    
    @IBAction func deleteAccount(_ sender: Any) {
        if (Reachability.isConnectedToNetwork()) {
            mDatabase.child("users").child(userID).removeValue()
            let firebaseAuth = Auth.auth()
            do {
                try firebaseAuth.signOut()
                performSegue(withIdentifier: "logout", sender: self)
            } catch let signOutError as NSError {
                print ("Error signing out: %@", signOutError)
            }
        } else {
            var message = MDCSnackbarMessage()
            message.text = "Collegare ad una rete Internet per visualizzare i dati."
            MDCSnackbarManager.show(message)
        }
    }
}
