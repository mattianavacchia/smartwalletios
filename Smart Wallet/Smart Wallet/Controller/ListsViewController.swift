//
//  ListsViewController.swift
//  Smart Wallet
//
//  Created by Mattia Navach on 22/06/2019.
//  Copyright © 2019 Progetto Mobile 2019. All rights reserved.
//

import UIKit
import Firebase

class ListsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var mDatabase: DatabaseReference!
    var userID: String = ""
    var lists = [ListTransactions]()

    @IBOutlet weak var tableViewLists: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        mDatabase = Database.database().reference()
        userID = (Auth.auth().currentUser?.uid)!
        
        // call to the DB
        let alert = UIAlertController(title: nil, message: "Aggiornamento in corso...", preferredStyle: .alert)
        
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
        
        getIDListFromUser()
    }
    
    @IBAction func addList(_ sender: Any) {
        showInputDialog()
    }
    
    func showInputDialog() {
        //Creating UIAlertController and
        //Setting title and message for the alert dialog
        let alertController = UIAlertController(title: "Crea nuova Lista", message: "Inserisci il nome", preferredStyle: .alert)
        
        //the confirm action taking the inputs
        let confirmAction = UIAlertAction(title: "Aggiungi", style: .default) { (_) in
            
            //getting the input values from user
            let name = alertController.textFields?[0].text
            if (name?.trim() != "") {
                let id = ListsManager.getNewID()
                self.mDatabase.child("lists").child(id).child("name").setValue(name)
                self.mDatabase.child("lists").child(id).child("people").child(self.userID).setValue(true)
                self.mDatabase.child("users").child(self.userID).child("lists").child(id).setValue(true)
                
                let l = ListTransactions()
                l.setId(id: id)
                l.setName(name: name!)
                self.lists.append(l)
                self.tableViewLists.reloadData()
            } else {
                print("Error with name")
            }
        }
        
        //the cancel action doing nothing
        let cancelAction = UIAlertAction(title: "Annulla", style: .cancel) { (_) in }
        
        //adding textfields to our dialog box
        alertController.addTextField { (textField) in
            textField.placeholder = "Nome"
        }
        
        //adding the action to dialogbox
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        
        //finally presenting the dialog box
        self.present(alertController, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.lists.count
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let cellSpacingHeight: CGFloat = 5
        return cellSpacingHeight
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let closeAction = UIContextualAction(style: .normal, title:  "Modifica", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            self.moveToSingle(index: indexPath.section)
            success(true)
        })
        closeAction.backgroundColor = UIColor(named: "colorModify")
        return UISwipeActionsConfiguration(actions: [closeAction])
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let closeAction = UIContextualAction(style: .normal, title:  "Elimina", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            if Reachability.isConnectedToNetwork() {
                self.mDatabase.child("lists").child(self.lists[indexPath.section].getId()).removeValue()
                self.mDatabase.child("users").child(self.userID).child("lists").child(self.lists[indexPath.section].getId()).removeValue()
                
                self.lists.remove(at: indexPath.section)
                self.tableViewLists.reloadData()
                success(true)
            } else {
                
            }
        })
        closeAction.backgroundColor = UIColor(named: "colorDelete")
        return UISwipeActionsConfiguration(actions: [closeAction])
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "lists_cell") as! ListTableViewCell
        cell.labelNameList.text = lists[indexPath.section].getName()
        let nameColor = "colorList" + String(Int.random(in: 0 ..< 8))
        print(nameColor)
        cell.backgroundColor = UIColor.init(named: nameColor)
        cell.layer.cornerRadius = 8
        cell.clipsToBounds = true
        cell.layer.borderColor = UIColor.black.cgColor
        cell.layer.borderWidth = 1
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        moveToSingle(index: indexPath.section)
    }
    
    private func moveToSingle(index: Int) {
        ListsManager.setCurrentList(l: self.lists[index])
        self.performSegue(withIdentifier: "lists_to_single", sender: self)
    }
    
    private func getIDListFromUser() {
        lists.removeAll()
        
        // mDatabase.child("users").child(mAuth.getUid()).child("lists");
        mDatabase.child("users").child(userID).child("lists").observeSingleEvent(of: .value, with: { (snapshot) in
            let enumerator = snapshot.children
            while let rest = enumerator.nextObject() as? DataSnapshot {
                self.getList(key: rest.key)
            }
            
            self.dismiss(animated: false, completion: nil)
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    private func getList(key: String) {
        // mDatabase.child("lists").child(id);
        mDatabase.child("lists").child(key).observeSingleEvent(of: .value, with: { (snapshot) in
            let enumerator = snapshot.children
            let l = ListTransactions()
            l.setId(id: key)
            while let rest = enumerator.nextObject() as? DataSnapshot {
                if (rest.key == "name") {
                    l.setName(name: rest.value as! String)
                }
            }
            
            self.lists.append(l)
            self.tableViewLists.reloadData()
        }) { (error) in
            print(error.localizedDescription)
        }
    }
}
