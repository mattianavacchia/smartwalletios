//
//  TabViewController.swift
//  Smart Wallet
//
//  Created by Mattia Navach on 05/06/2019.
//  Copyright © 2019 Progetto Mobile 2019. All rights reserved.
//

import UIKit
import Firebase

class TabViewController: UITabBarController, UITabBarControllerDelegate {
    
    public var imageSelected: UIImage!
    public var imageUrl: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if TransactionManager.getTypeOperation() == TypeOperation.OCR {
            if imageSelected != nil { // OCR
                performOCR()
            } else {
                // NOTIFY
            }
        } else {
            if TransactionManager.getTypeOperation() == TypeOperation.MODIFY { // MODIFY
            } else { // EMPTY
                TransactionManager.initializeTransaction()
            }
        }
    }
    
    // UITabBarDelegate
    /*override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if item == (self.tabBar.items as! [UITabBarItem])[2]{
            // upload
        }
    }
    
    // UITabBarControllerDelegate
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        print("Selected view controller")
    }*/

    private func performOCR() {
        if (imageUrl != nil || imageUrl == "") {
            TransactionManager.setPathPhoto(path: imageUrl)
        }
        let vision = Vision.vision()
        let textRecognizer = vision.onDeviceTextRecognizer()
        let ocrManager = OcrManager.init()
        
        let visionImage = VisionImage(image: imageSelected)
        
        textRecognizer.process(visionImage) { result, error in
            guard error == nil, let result = result else {
                print("Error with OCR")
                return
            }
            //print(result)
            let amount = Double(ocrManager.processTotal(result: result))
            if amount != nil {
                TransactionManager.setAmount(amount: amount!)
            }
            TransactionManager.setAmount(amount: Double(ocrManager.processTotal(result: result))!)
            TransactionManager.setShopAddress(shopAddress: ocrManager.getAddress(result: result))
            TransactionManager.setShopName(shopName: ocrManager.getName(result: result))
            TransactionManager.setDate(date: ocrManager.getDate(result: result))
            TransactionManager.setDescription(description: ocrManager.getDetails(result :result))
        }
    }
}


